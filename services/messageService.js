const { messageRepository } = require('../repositories/messageRepository')
const {isHaveSpaces,IsEmpty}  = require("./validation/validation")

class MessageService {
    search(userData) {
        if(!userData.hasOwnProperty("id"))
            throw Error("Missing id field")
        isHaveSpaces(userData) 
        IsEmpty(userData)
        const item = messageRepository.getOne({id:userData.id})
        if(!item) {
            throw Error("User not found");
        }
        return item;
    }
    create(userData){
        if(!userData.hasOwnProperty("userId"))
            throw Error("Missing userId field")

        if(!userData.hasOwnProperty("createdAt"))
            throw Error("Missing createdAt field")

        if(!userData.hasOwnProperty("text"))
            throw Error("Missing text field")

        isHaveSpaces(userData) 
        IsEmpty(userData)
        const message =  {
            userId: userData.userId,
            text: userData.text,
            createdAt:userData.createdAt
        }
        messageRepository.create(message)
        return message
    }
    delete(userData) {
        if(!userData.hasOwnProperty("id"))
            throw Error("Missing id field")
        isHaveSpaces(userData) 
        IsEmpty(userData)
        const data =  messageRepository.getOne({id: userData.id})
        if(!data)
            throw Error("User not found");
        return messageRepository.delete(userData.id)[0]
    }
    update(userData){
        if(!userData.hasOwnProperty("id"))
            throw Error("Missing id field")
        if(!userData.hasOwnProperty("text"))
            throw Error("Missing text field")
        if(!userData.hasOwnProperty("editedAt"))
            throw Error("Missing editedAt field")
        isHaveSpaces(userData) 
        IsEmpty(userData)
        const user = messageRepository.update(userData.id,userData);
        return user
    }
    getAll(){
        const all = messageRepository.getAll()
        if(!all)
         throw Error("List messages is empty")
        return all
    }
    deleteAllByUser(userData) {
        if(!userData.hasOwnProperty("userId"))
            throw Error("Missing userId field")
        isHaveSpaces(userData) 
        IsEmpty(userData)
        const data =  messageRepository.getOne({userId: userData.userId})
        if(!data)
            return [];
        return messageRepository.deleteUsersMessages(userData.userId);
    }
}

module.exports = new MessageService()