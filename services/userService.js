const { UserRepository } = require('../repositories/userRepository')
const MessageService = require('../services/messageService')
const {isHaveSpaces,IsEmpty,isMissingCreateFields,isMissingemailFields}  = require("./validation/validation")

class UserService {
    read(userData){
        if(!userData.hasOwnProperty("userId"))
            throw Error("Missing userId field")
        isHaveSpaces(userData) 
        IsEmpty(userData)
        const item = UserRepository.getOne({userId:userData.userId})
        if(!item) {
            throw Error("User not found");
        }
        return {
            user:item.user,
            avatar:item.avatar
        }
    }
    login(userData) {
        isMissingemailFields(userData)
        isHaveSpaces(userData) 
        IsEmpty(userData)
        const item = UserRepository.getOne({email:userData.email,password:userData.password})
        if(!item) {
            throw Error("User not found");
        }
        return {
            userId:item.userId,
            isAdmin:item.isAdmin,
            user:item.user,
            avatar:item.avatar}
    }
    register(userData){
        if(UserRepository.getOne({email: userData.email}))
            throw Error("This email is already taken")
        if(UserRepository.getOne({user: userData.user}))
            throw Error("This user is already taken")
        isMissingCreateFields(userData)
        isHaveSpaces(userData) 
        IsEmpty(userData)

        const newUser = {
            isAdmin:false,
            email: userData.email,
            password: userData.password,
            user: userData.user,
            avatar:"https://miro.medium.com/max/720/1*W35QUSvGpcLuxPo3SRTH4w.png"
        }
        UserRepository.create(newUser)
        return newUser
    }
    delete(userData) {
        if(!userData.hasOwnProperty("userId"))
            throw Error("Missing userId field")
        isHaveSpaces(userData) 
        IsEmpty(userData)
    
        const data =  UserRepository.getOne({userId: userData.userId})
        if(!data)
            throw Error("User not found");
        MessageService.deleteAllByUser(userData);
        return UserRepository.delete(userData.userId)[0]
    }
    update(userData){
        if(!userData.hasOwnProperty("userId"))
            throw Error("Missing userId field")
        isHaveSpaces(userData) 
        IsEmpty(userData)
        const user = UserRepository.update(userData.userId,userData);
        return user
    }
    getAll(){
        const all = UserRepository.getAll()
        return all
    }
}

module.exports = new UserService()