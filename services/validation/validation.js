function isMissingCreateFields(item){
    if(!item.hasOwnProperty("user"))
        throw Error("Missing user field")
    if(!item.hasOwnProperty("email"))
        throw Error("Missing email field")
    if(!item.hasOwnProperty("password"))
        throw Error("Missing password field")
}
function isMissingemailFields(item){
    if(!item.hasOwnProperty("email"))
        throw Error("Missing email field")
    if(!item.hasOwnProperty("password"))
        throw Error("Missing password field")
}


function IsEmpty(item){
    Object.keys(item).forEach(key=>{ 
        if(item[key]==="")
            throw Error(`Empty ${key} field`)
    })
}
function isHaveSpaces(item){
    Object.keys(item).forEach(key=>{ 
        if(item[key].includes(" ")&&key!=="text"&&key!=="createdAt"&&key!=="editedAt")
            throw Error(`Field ${key} have spaces `)
    })
}
module.exports = {IsEmpty,isHaveSpaces,isMissingCreateFields,isMissingemailFields}