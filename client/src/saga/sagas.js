import { takeEvery, put, call } from "@redux-saga/core/effects";


import {
  preloaderOn,
  preloaderOff,
  errorModalOff,
  errorModalOn,
  
  
  fetchListUsers,
  fetchListMessages,
  login,
  addMessage,
  deleteMessage,
  deleteUser,
  EditUser,
  editMessage,
  register
} from "../reducer/actions";
import * as fetchFunc from "./fetchFunction";

export function* sagaWatcher() {
  yield takeEvery(fetchListMessages.REQUEST, sagaGetMessages);
  yield takeEvery(login.REQUEST, sagaTryLogin);
  yield takeEvery(addMessage.REQUEST, sagaAddMessage);
  yield takeEvery(deleteMessage.REQUEST, sagaDeleteMessage);
  yield takeEvery(fetchListUsers.REQUEST, sagaFetchUsers);
  yield takeEvery(editMessage.REQUEST, sagaEditMessage);
  yield takeEvery(register.REQUEST, sagaAddUser);
  yield takeEvery(EditUser.REQUEST, sagaEditUser);
  yield takeEvery(deleteUser.REQUEST, sagaDeleteUser);
}

function* sagaDeleteUser(action) {
  try {
    yield put(preloaderOn());
    const payload = yield call(fetchFunc.fetchDeleteUser, action.payload);
    if (payload.error) {
      yield put(preloaderOff());
      yield put(errorModalOn(payload.message));
    } else {
      yield put(errorModalOff());
      const messages = yield call(fetchFunc.fetchMessages);
      yield put(fetchListMessages.success(messages));
      yield put(deleteUser.success(payload));
      yield put(preloaderOff());
    }
  } catch (e) {
    yield put(preloaderOff());
  }
}

function* sagaAddUser(action) {
  try {
    yield put(preloaderOn());
    const payload = yield call(fetchFunc.fetchAddUser, action.payload);
    if (payload.error) {
      yield put(preloaderOff());
      yield put(errorModalOn(payload.message));
    } else {
      yield put(preloaderOff());
      yield put(register.success(payload));
      const element = document.querySelector(".message-list");
      if (element) {
        element.scroll({
          top: element.scrollHeight,
          behavior: "smooth",
        });
      }
    }
  } catch (e) {
    yield put(preloaderOff());
  }
}

function* sagaEditUser(action) {
  try {
    yield put(preloaderOn());
    const payload = yield call(fetchFunc.fetchPutUser, action.payload);
    if (payload.error) {
      yield put(preloaderOff());
      yield put(errorModalOn(payload.message));
    } else {
      yield put( EditUser.success(payload) )
      // const messages = yield call(fetchFunc.fetchMessages);
      // const users = yield call(fetchFunc.fetchUsers);
      // yield put(fetchListUsers.success(users));
      // yield put(fetchListMessages.success(messages));
      yield put(errorModalOff());
      yield put(preloaderOff());
    }
  } catch (e) {
    yield put(preloaderOff());
  }
}

function* sagaEditMessage(action) {
  try {
    yield put(preloaderOn());
    const payload = yield call(fetchFunc.fetchPutMessage, action.payload);
    if (payload.error) {
      yield put(preloaderOff());
      yield put(errorModalOn(payload.message));
    } else {
      yield put(errorModalOff());
      yield put(editMessage.success(action.payload));
      yield put(preloaderOff());
    }
  } catch (e) {
    yield put(preloaderOff());
  }
}
function* sagaFetchUsers() {
  try {
    yield put(preloaderOn());
    const payload = yield call(fetchFunc.fetchUsers);
    if (payload.error) {
      yield put(preloaderOff());
      yield put(errorModalOn(payload.message));
    } else {
      yield put(fetchListUsers.success(payload));
      yield put(preloaderOff());
    }
  } catch (e) {
    yield put(preloaderOff());
  }
}
function* sagaAddMessage(action) {
  try {
    yield put(preloaderOn());
    const payload = yield call(fetchFunc.fetchAddMessage, action.payload);
    if (payload.error) {
      yield put(preloaderOff());
      yield put(errorModalOn(payload.message));
    } else {
      yield put(errorModalOff());
      yield put(addMessage.success(payload));
      yield put(preloaderOff());
      const element = document.querySelector(".message-list");
      if (element) {
        element.scroll({
          top: element.scrollHeight,
          behavior: "smooth",
        });
      }
    }
  } catch (e) {
    yield put(preloaderOff());
  }
}
function* sagaDeleteMessage(action) {
  try {
    yield put(preloaderOn());
    const payload = yield call(fetchFunc.fetchDeleteMessage, action.payload);
    if (payload.error) {
      yield put(preloaderOff());
      yield put(errorModalOn(payload.message));
    } else {
      yield put(errorModalOff());
      yield put(deleteMessage.success(action.payload));
      yield put(preloaderOff());
    }
  } catch (e) {
    yield put(preloaderOff());
  }
}
function* sagaTryLogin(action) {
  try {
    yield put(preloaderOn());
    const payload = yield call(fetchFunc.fetchLoginStatus, action.payload);
    if (payload.error) yield put(login.failure(payload));
    else {
      yield put(login.success(payload));
      const messages = yield call(fetchFunc.fetchMessages);
      const users = yield call(fetchFunc.fetchUsers);
      yield put(fetchListUsers.success(users));
      yield put(fetchListMessages.success(messages));
    }
    yield put(preloaderOff());
  } catch (e) {
    yield put(login.failure(e));
    yield put(preloaderOff());
  }
}
function* sagaGetMessages() {
  try {
    yield put(preloaderOn());
    const payload = yield call(fetchFunc.fetchMessages);
    if (payload.error) {
      yield put(preloaderOff());
      yield put(errorModalOn(payload.message));
    } else {
      yield put(fetchListMessages.success(payload));
      yield put(preloaderOff());
    }
  } catch (e) {
    yield put(preloaderOff());
  }
}
