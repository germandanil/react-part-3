export async function fetchLoginStatus(payload) {
    const response = await postData("/api/user/login", payload)
    return response
  }
  export async function fetchAddMessage(message) {
    const response = await postData("/api/message/create", {userId:message.userId,text:message.text,createdAt:message.createdAt})
    return response
  }
  export async function fetchAddUser(message) {
    const response = await postData("/api/user/register", {...message})
    return response
  }
  export async function fetchDeleteMessage(message) {
    const response = await deleteData("/api/message/delete", {id:message.id})
    return response
  }
  export async function fetchMessages() {
    const response = await fetch("/api/message/all")
    return await response.json()
  }
  export async function fetchUsers() {
    const response = await fetch("/api/user/all")
    return await response.json()
  }
  export async function fetchPutMessage(data) {
    const response = await putData("/api/message/update", data);
    return response
  }
  export async function fetchPutUser(data) {
    const response = await putData("/api/user/update", {...data});
    return response
  }
  export async function fetchDeleteUser(user){
    const response = await deleteData("/api/user/delete", {userId:user.userId})
    return response
  }

  export async function deleteData(url , data) {
    const response = await fetch(url, {
      method: 'DELETE', 
      mode: 'cors', 
      cache: 'no-cache', 
      credentials: 'same-origin', 
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow', 
      referrerPolicy: 'no-referrer', 
      body: JSON.stringify(data) 
    });
    return response.json(); 
  }
  export async function postData(url , data) {
    const response = await fetch(url, {
      method: 'POST', 
      mode: 'cors', 
      cache: 'no-cache', 
      credentials: 'same-origin', 
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow', 
      referrerPolicy: 'no-referrer', 
      body: JSON.stringify(data) 
    });
    return response.json(); 
  }
export async function putData(url , data) {
    const response = await fetch(url, {
      method: 'PUT', 
      mode: 'cors', 
      cache: 'no-cache', 
      credentials: 'same-origin', 
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow', 
      referrerPolicy: 'no-referrer', 
      body: JSON.stringify(data) 
    });
    return response.json(); 
  }