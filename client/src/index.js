import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import Preloader from "./components/Preloader";
import { Provider} from "react-redux";
import store from "./store";
import ErrorModal from "./components/error/errorModal"


ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <Preloader/>
      <ErrorModal/>
      <App />
    </React.StrictMode>
  </Provider>,

  document.getElementById("root")
);

reportWebVitals();
