import { createStore,compose,applyMiddleware } from "redux";
import rootReducer from "./reducers";
import createSagaMiddleware from "redux-saga"
import { sagaWatcher } from "./saga/sagas";

const saga = createSagaMiddleware()
const store = createStore(rootReducer,compose( applyMiddleware(saga),window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));
saga.run(sagaWatcher )
export default store;
