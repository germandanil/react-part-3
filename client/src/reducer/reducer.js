import { login, fetchListUsers, fetchListMessages, addMessage, deleteMessage, deleteUser, editMessage, register, EditUser } from "./actions";
import {
  PRELOADER_OFF,
  PRELOADER_ON,

  ERROR_MODAL_ON,
  ERROR_MODAL_OFF
} from "./actionTypes";

const _ = require("lodash");

const initialState = {
  auth:{
    isAuthorized:false,
    adminStatus:false,
    error:{
      isError:false,
      errorText:""
    }
  },
  chat: {
    ownId:"",
    onwAvatar:"",
    ownName:"",
    messages: [],
  },
  preloader: false,

  error:{
    isError:false,
    errorText:""
  },
  users:[]
};

export default function chat(state = initialState, action) {
  switch (action.type) {
    case PRELOADER_OFF: {
      return {
        ...state,
        preloader: false,
      };
    }
    case PRELOADER_ON: {
      return {
        ...state,
        preloader: true,
      };
    }
    case register.SUCCESS: {
      const usersList = [...state.users]
      usersList.push(action.payload);
      return {
        ...state,
        users: usersList
      };
    }
    case EditUser.SUCCESS:{
      const userId = action.payload.userId
      const usersList = state.users.filter(
        (p) => p.userId !== userId
      );
      usersList.push(action.payload);
      return {
        ...state,
        users: usersList
      };
    }
    case fetchListUsers.SUCCESS:
      return {
      ...state,
      users:action.payload
    }
    case fetchListMessages.SUCCESS: {
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: action.payload,
        },
      };
    }
    case addMessage.SUCCESS: {
      const newMessages = state.chat.messages.map((message) => Object.assign({}, message));
      const message = {
        userId: action.payload.userId,
        id: action.payload.id,
        avatar: action.payload.avatar,
        name: action.payload.user,
        text: action.payload.text,
        createdAt: action.payload.createdAt,
      };
      newMessages.push(message);
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: newMessages,
        },
      };
    }
    case deleteMessage.SUCCESS: {
      const newMessages = state.chat.messages.filter(
        (m) => m.id !== action.payload.id
      );
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: newMessages,
        },
      };
    }
    case deleteUser.SUCCESS: {
      const newUsersList = state.users.filter(
        (p) => p.userId !== action.payload.userId
      );
      return {
        ...state,
        users:newUsersList
      };
    }

    case editMessage.SUCCESS: {
      let newMessages = [...state.chat.messages];
      newMessages = newMessages.map((message) => Object.assign({}, message));

      const editedMessage = _.find(newMessages, {
        id: action.payload.id,
      });
      editedMessage.text = action.payload.text;
      editedMessage.editedAt = action.payload.editedAt;
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: newMessages,
        },
      };
    }
    case login.SUCCESS: {
      return {
        ...state,
        auth: {
          ...state.auth,
          isAuthorized:true,
          adminStatus:action.payload.isAdmin
        },
        chat:{
          ...state.chat,
          ownId:action.payload.userId,
          onwAvatar:action.payload.avatar,
          ownName:action.payload.user,
        }
      };
    }
    case login.FAILURE: {
      return {
        ...state,
        auth: {
          ...state.auth,
          error:{
            isError:true,
            errorText:action.payload.message  
          }
        },
      };
    }
    case ERROR_MODAL_ON:{
      return {
        ...state,
        error:{
          isError:true,
          errorText:action.payload.text
        }
      };
    }
    case ERROR_MODAL_OFF:{
      return {
        ...state,
        error:{
          isError:false,
          errorText:""
        }
      };
    }

    default:
      return state;
  }
}


