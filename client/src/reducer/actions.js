import {
  PRELOADER_OFF,
  PRELOADER_ON,
  ERROR_MODAL_ON,
  ERROR_MODAL_OFF,
  REGISTER,
  EDIT_USER,
  EDIT_MESSAGE,

  LOGIN,
  FETCH_MESSAGES_LIST,
  FETCH_USERS_LIST,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  DELETE_USER
} from "./actionTypes";
import { createRoutine } from 'redux-saga-routines';
 


export const preloaderOn = () => {
  return {
    type: PRELOADER_ON,
  };
};
export const preloaderOff = () => {
  return {
    type: PRELOADER_OFF,
  };
};

export const fetchListMessages = createRoutine( FETCH_MESSAGES_LIST )
export const fetchListUsers = createRoutine( FETCH_USERS_LIST )
export const addMessage = createRoutine( ADD_MESSAGE)

export const EditUser= createRoutine(EDIT_USER)
export const editMessage= createRoutine(EDIT_MESSAGE)
 
export const deleteMessage = createRoutine( DELETE_MESSAGE)
 

export const login= createRoutine(LOGIN)

export const errorModalOn= (text)=>{
  return{
    type:ERROR_MODAL_ON,
    payload: {
      text
    },
  }
}
export const errorModalOff= (text)=>{
  return{
    type:ERROR_MODAL_OFF
  }
}

  export const register= createRoutine( REGISTER)

  export const deleteUser = createRoutine( DELETE_USER)
