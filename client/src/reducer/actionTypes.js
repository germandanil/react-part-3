export const PRELOADER_OFF = "PRELOADER_OFF";
export const PRELOADER_ON = "PRELOADER_ON";
export const DELETE_MESSAGE = "DELETE_MESSAGE";
export const OPEN_EDITOR = "OPEN_EDITOR";
export const EDIT_MESSAGE ="EDIT_MESSAGE"
export const ADD_MESSAGE = "ADD_MESSAGE";
export const email_SUCCESS ="email_SUCCESS"
export const email_ERROR ="email_ERROR"
export const ERROR_MODAL_OFF ="ERROR_MODAL_OFF"
export const ERROR_MODAL_ON ="ERROR_MODAL_ON"
export const SET_USERS = "SET_USERS"
export const REGISTER= "REGISTER"
export const EDIT_USER= "EDIT_USER"
export const DELETE_USER= "DELETE_USER"



export const LOGIN ="LOGIN"
export const FETCH_USERS_LIST= "FETCH_USERS_LIST"
export const FETCH_MESSAGES_LIST = "FETCH_MESSAGES_LIST";
