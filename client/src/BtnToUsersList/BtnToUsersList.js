import React from "react"

import {
    NavLink
  } from "react-router-dom";
export default function BtnToUsersList(){

        return(
            <NavLink to = "/user/list" className = "btn-to-user">
                User List
            </NavLink>
        )
} 
