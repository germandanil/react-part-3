import React from "react";
import Message from "./Message";
import DateLine from "./Dateline";

export default function MessageList({messages,ownID}) {
    let list = [];
    if (messages && messages.length !== 0) {
      const currentMessages = messages ?? [];
      const currentOwnID = ownID ?? 0;
      const dayMilliseconds = 24 * 60 * 60 * 1000;
      let lastDate = new Date(
        Date.parse(messages[messages.length - 1].createdAt) - dayMilliseconds
      ); // date before oldest message
      let currentData;
      currentMessages.forEach((m) => {
        currentData = new Date(m.createdAt);
        if (
          lastDate.getDate() !== currentData.getDate() ||
          lastDate.getFullYear() !== currentData.getFullYear() ||
          lastDate.getMonth() !== currentData.getMonth()
        ) {
          list.push(
            <DateLine
              key={currentData.toLocaleString("en-US")}
              date={currentData}
            />
          );
          lastDate = currentData;
        }
        list.push(
          <Message key={m.id} message={m} isOwn={currentOwnID === m.userId} />
        );
      });
    }
    return <div className="message-list">{list}</div>;
}
