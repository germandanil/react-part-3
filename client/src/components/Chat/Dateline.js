import React,{useMemo} from "react";
function convertDate(date) {
  const dayMilliseconds = 24 * 60 * 60 * 1000;
  const today = new Date().setHours(0, 0, 0, 0);
  if (date > today) return "Today";
  else if (date > today - dayMilliseconds) return "Yesterday";
  else {
    const weekday = date.toLocaleString("en-DE", { weekday: "long" });
    const month = date.toLocaleString("en-DE", { month: "long" });
    const day = date.toLocaleString("en-DE", { day: "numeric" });
    return weekday + ", " + day + " " + month;
  }
}
export default function DateLine({date}){

    const currentDate = useMemo(()=>convertDate(date),[date]);
    return (
      <div className="line">
        <div className="line-wrp">
          <hr />
        </div>
        <div className="messages-divider">{currentDate}</div>
      </div>
    );
}
