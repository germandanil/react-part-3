import React,{useMemo} from "react";

function convertDate(date) {
  const mounthZero = date.getMonth() + 1 < 10 ? "0" : "";
  const dateZero = date.getDate() < 10 ? "0" : "";
  const hoursZero = date.getHours() < 10 ? "0" : "";
  const minutesZero = date.getMinutes() < 10 ? "0" : "";
  return `${hoursZero + date.getHours()}:${minutesZero + date.getMinutes()} ${
    dateZero + date.getDate()
  }.${mounthZero + (date.getMonth() + 1)}.${date.getFullYear()}`;
}

export default function Header({messages,title}){
    const currentMessages = messages ?? [];
    const numMessage = currentMessages.length ?? 0;
    const date =useMemo(()=>{
      if(numMessage)
        return convertDate( new Date(currentMessages[numMessage - 1].createdAt) )
      else
       return "about 65 million years ago, in the morning";
    },[currentMessages,numMessage]);
    const users = new Set();

    currentMessages.forEach((m) => {
      users.add(m.userId);
    });
    const numUsers = users.size;
    return (
      <div className="header">
        <div className="header-title">{title}</div>
        <div className="header-users-count">{numUsers + " participants"} </div>
        <div className="header-messages-count">{numMessage + " messages"}</div>
        <div className="header-last-message-date">
          {"last message at   " + date}
        </div>
      </div>
    );
}
