import React, { useState } from "react";
import {  useDispatch, useSelector } from "react-redux";
import { addMessage } from "../../reducer/actions";

export default function MessageInput() {
  const dispatch = useDispatch()
  const userId = useSelector(state=>state.chat.ownId)
  const avatar = useSelector(state=>state.chat.onwAvatar)
  const name = useSelector(state=>state.chat.ownName)
  const [value,setValue] = useState("")
  const handleChange =  (event) =>{
    setValue( event.target.value );
  }
  const handleEnter = (event)=> {
    if (event.key === "Enter") 
      handleSubmit(event);
  }
  const handleSubmit = (event)=> {
    const createdAt = new Date().toString();
    if (value !== "") {
      dispatch(addMessage.request({text:value, createdAt,userId,name,avatar}));
      setValue( "" );
    }
    event.preventDefault();
  }

    return (
      <div className="message-input">
        <form onSubmit={handleSubmit}>
          <label>
            <textarea
              className="message-input-text"
              value={value}
              onChange={handleChange}
              onKeyDown={handleEnter}
            >
            </textarea>
          </label>
          <input
            className="message-input-button"
            type="submit"
            value={"Send"}
          />
        </form>
      </div>
    );
}


