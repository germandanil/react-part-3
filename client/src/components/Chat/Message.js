import React, { useState } from "react";
import { useDispatch,useSelector} from "react-redux";
import { deleteMessage} from "../../reducer/actions";

import {
  NavLink
} from "react-router-dom";

export default function Message({message,isOwn}) {

  const adminStatus = useSelector(state=>state.auth.adminStatus);

  const [isLiked,setLiked] = useState(false)
  const dispatch = useDispatch();
  function onClick() {
    setLiked(!isLiked);
  }
  function convertDate(date) {
    const hoursZero = date.getHours() < 10 ? "0" : "";
    const minutesZero = date.getMinutes() < 10 ? "0" : "";
    return `${hoursZero + date.getHours()}:${minutesZero + date.getMinutes()}`;
  }
  function convertDateForEditDate(date) {
    const mounthZero = date.getMonth() + 1 < 10 ? "0" : "";
    const dateZero = date.getDate() < 10 ? "0" : "";
    return `${convertDate(date)} ${dateZero + date.getDate()}.${
      mounthZero + (date.getMonth() + 1)
    }.${date.getFullYear()}`;
  }
  function deleteCurrentMessage() {
    dispatch(deleteMessage.request({id:message.id}));
  }
    let tools;
    let user = false;
    

    if(!isOwn){
      tools = (
        <div className="tools">
          <div className="byrger"></div>
          <i
            onClick={onClick}
            className={
              (isLiked ? "message-liked" : "message-like") +
              " fas fa-heart"
            }
          ></i>
        </div>
      );
      user = (
        <div className="user-message-wrp">
          <img
            className="message-user-avatar"
            src={message.avatar}
            alt={"user-img-" + message.user}
          />
          <div className="message-user-name">{message.user}</div>
        </div>
      );
    }
    if (isOwn||adminStatus)
    tools = (
      <div className="tools">
        <NavLink to={`/edit/message/${message.id}`}>
        <i
          className="message-edit fas fa-pencil-alt"
        ></i>
        </NavLink>
        <i
          onClick={deleteCurrentMessage}
          className="fas fa-trash-alt message-delete"
        ></i>
      </div>
    );

    let date = convertDate(new Date(message.createdAt));
    if (message.editedAt) {
      date = "edited at " + convertDateForEditDate(new Date(message.editedAt));
    }
    return (
      <div className={isOwn ? "own-message" : "message"}>
        <div className="message-wrp">
          {user}
          <div className="main-message-wrp">
            <p className="message-text">{message.text} </p>
            <div className="message-time">{date}</div>
          </div>
          {tools}
        </div>
      </div>
    );
}


