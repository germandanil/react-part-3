import React, { useEffect } from "react";

import {useSelector } from "react-redux";


import Header from "./Header";
import MessageList from "./Messagelist";
import MessageInput from "./Input";
import { useHistory } from "react-router-dom";



export default function Chat(){
const history = useHistory();
const messages = useSelector(state=> state.chat.messages);
const editModal = useSelector(state=> state.chat.editModal);
const MY_ID = useSelector(state=> state.chat.ownId);
  useEffect(()=>{
        window.addEventListener("keyup", onArrowEditLastOwnMessage)
        return ()=>{
          window.removeEventListener("keyup", onArrowEditLastOwnMessage)
        }
  });
  function getLastOwnMessage() {
    let message = null;
    messages.forEach((m) => {
      if (m.userId === MY_ID) message = m;
    });
    return message;
  }
  function onArrowEditLastOwnMessage(event) {
    if (event.key === "ArrowUp") {
      let lastMessage = getLastOwnMessage();
      if (lastMessage && !editModal) {
        history.push(`/edit/message/${lastMessage.id}`);
      }
    }
  }

    return <div className="chat">
      <Header
        messages={messages}
        title="Hidden pull"
      />
      <MessageList
        messages={messages}
        ownID={MY_ID}
      />
      <MessageInput/>
      </div>
}

