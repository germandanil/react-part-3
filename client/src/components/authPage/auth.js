import React,{useState} from "react"
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../reducer/actions";

export default function Auth() {
    const [email, setemail] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch()
    function handleSubmit(event) {
        dispatch(login.request({email,password}))
        event.preventDefault();
    }
    function onChangeUserName(value){
        setemail(value);
    }
    function onChangePassword(value){
        setPassword(value);
    }
    let errorBody = false;
    const isError = useSelector(state=>state.auth.error.isError)
    const errorText = useSelector(state=>state.auth.error.errorText)
    if(isError)
        errorBody = (
            <div className = "login-error">
                <span className = "login-error-text">{errorText}</span>
            </div>)
        
    return (
        <div className = "login">
            <form onSubmit={handleSubmit}>
            <label>
            <p>email</p>
            <input type="text" onChange={e => onChangeUserName(e.target.value)} />
            </label>
            <label>
            <p>Password</p>
            <input type="password" onChange={e => onChangePassword(e.target.value)} />
            </label>
            <div>
            <button type="submit">Submit</button>
            </div>
            </form>
            {errorBody}
      </div>
    );
  }