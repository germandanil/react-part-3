import React from "react";
import { useSelector } from "react-redux";
export default function Preloader(){
    const preloader = useSelector(state=>state.preloader);
    if(preloader){
      return (
        <div className="preloader">
          <i className="spinner fas fa-spinner fa-spin"></i>
        </div>
      );
    }
    return false

}
