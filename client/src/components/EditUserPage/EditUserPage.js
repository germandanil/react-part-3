import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { EditUser, errorModalOn } from "../../reducer/actions";
import {
    NavLink
  } from "react-router-dom";
const _ = require("lodash")

export default function EditUserPage(){
    let history = useHistory();
    const userIdParams =useParams().id;
    const user = _.find(useSelector(state=>state.users),(user)=>user.userId===userIdParams)



    const [email,setEmail] = useState(user.email);
    const [name,setName] = useState(user.user);
    const [password,setPassword] = useState(user.password);
    const dispatch = useDispatch()

    function onChangeEmail(value){
        setEmail(value);
    }
    function onChangeName(value){
        setName(value);
    }
    function onChangePassword(value){
        setPassword(value);
    }
    function handleSubmit(event) {
        let editData=  {userId:user.userId}

        if(name !==user.user)
            editData={...editData,user:name}
        if(email !==user.email)
            editData={...editData,email}
        if(password !==user.password)
            editData={...editData,password}

        if(Object.keys(editData).length===1){
            history.push("/user/list")
            dispatch(errorModalOn("All field was empty, request update data denied"))
        }
        else{
            history.push("/user/list");
            dispatch(EditUser.request(editData))
        }
        event.preventDefault();
    }
    return (
        <div className = "edit-user-page">
            <NavLink to =  "/user/list" className = "btn-back-to-user-list">Back to User List</NavLink>
            <form onSubmit={handleSubmit}>
            <label>
            <p>User</p>
            <input type="text" value = {name} onChange={e => onChangeName(e.target.value)} />
            </label>
            <label>
            <p>Email</p>
            <input type="text" value = {email} onChange={e => onChangeEmail(e.target.value)} />
            </label>
            <label>
            <p>Password</p>
            <input type="text" value = {password} onChange={e => onChangePassword(e.target.value)} />
            </label>
            <div>
            <button type="submit">EDIT</button>
            </div>
            </form>
      </div>
    );
  }