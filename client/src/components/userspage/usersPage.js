import React from "react"
import {
    NavLink
  } from "react-router-dom";
import UsersList from "./usersList"
export default function UsersPage() {
    return(<>
            <div className = "btn-user-wrp">
            <NavLink to =  "/chat" className = "btn-back-user">Back to Chat</NavLink>
            <NavLink to =  "/user/add" className = "btn-add-user">Add new user</NavLink>
            </div>
            <UsersList/>
        </>
    )
}