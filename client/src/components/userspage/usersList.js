import React from "react"
import { useSelector } from "react-redux"
import UserCard from "./userCard";
export default function UsersList(){
    const users = useSelector(state=>state.users);
    return(
    <div className = "users-list-wrp">
        <div className = "users-list">
            {users.map(user=>{
                if(!user.isAdmin)
                    return (<UserCard user= {user} key = {user.userId}/>)
                return false;
            })}
        </div>
    </div>
    )
}