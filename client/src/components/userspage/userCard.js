import React from "react";
import { useDispatch } from "react-redux";
import {  deleteUser } from "../../reducer/actions";
import {
    NavLink
  } from "react-router-dom";
export default function UserCard({user}) {
    const dispatch = useDispatch()

    function onDeleteUser(){
        dispatch(deleteUser.request({userId:user.userId}));
    }
    return(
        <div className = "user-card">
            <div className = "user-name-email">
                <span className= "user-name">{user.user}</span>
                <span className= "user-email">{user.email}</span>
            </div>
            <div className= "btn-card-user-wrp">
            <NavLink to ={`/user/edit/${user.userId}`} className = "btn-edit-user">Edit</NavLink>
            <button className ="btn-delete-user" onClick = {onDeleteUser}>Delete</button>
            </div>
        </div>
    )
}