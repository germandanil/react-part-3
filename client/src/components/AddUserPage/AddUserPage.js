import React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { register } from "../../reducer/actions";
import {
    NavLink
  } from "react-router-dom";
export default function AddUserPage(){
    const history = useHistory();
    const [email,setEmail] = useState("");
    const [name,setName] = useState("");
    const [password,setPassword] = useState("");
    const dispatch = useDispatch()
    function onChangeEmail(value){
        setEmail(value);
    }
    function onChangeName(value){
        setName(value);
    }
    function onChangePassword(value){
        setPassword(value);
    }
    function handleSubmit(event) {
        dispatch(register.request({user:name,email,password}))
        history.push("/user/list");
        event.preventDefault();
    }
    return (
        <div className = "add-user-page">
            <NavLink to = "/user/list"className = "btn-back-to-user-list" >BACK</NavLink>
            <form onSubmit={handleSubmit}>
            <label>
            <p>User</p>
            <input type="text" onChange={e => onChangeName(e.target.value)} />
            </label>
            <label>
            <p>Email</p>
            <input type="text" onChange={e => onChangeEmail(e.target.value)} />
            </label>
            <label>
            <p>Password</p>
            <input type="text" onChange={e => onChangePassword(e.target.value)} />
            </label>
            <div>
            <button type="submit">ADD</button>
            </div>
            </form>
      </div>
    );
  }