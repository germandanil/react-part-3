import React, { useState } from "react";
import {  useDispatch, useSelector } from "react-redux";
import {
  NavLink, useHistory, useParams
} from "react-router-dom";
import {  editMessage } from "../../reducer/actions";
const _ = require("lodash")

export default function EditPage() {
  const dispatch  = useDispatch()
  let history = useHistory();
  const messageId =useParams().id;
  const message = _.find(useSelector(state=>state.chat.messages),(message)=>message.id===messageId)
  let [value,setState] = useState(message.text);
  function handleChange(event) {
    setState( event.target.value);
  }
  function handleEnter(event) {
    if (event.key === "Enter") 
      handleSubmit(event);
  }
  function handleSubmit(event) {
    history.push("/chat");
    const date = new Date().toString();
    
    if (value !== message.text)
      dispatch(editMessage.request({text:value, editedAt:date,id:messageId}));
    event.preventDefault();
  }
  return (
    <div className={"edit-message-modal modal-shown"}>
      <div className="edit-message-modal-body">
        <div className="close-btn-wrp">
          <NavLink exact to = "/chat" className="edit-message-close">
          <i className="fas fa-times-circle" ></i>
          </NavLink>
        </div>
        <form onSubmit={handleSubmit}>
          <label>
            <textarea
              className="edit-message-input"
              value={value}
              onChange={handleChange}
              onKeyDown={handleEnter}
            >
            </textarea>
          </label>
          <div className="btn-save-wrp">
            <input
              className="edit-message-button"
              type="submit"
              value="Save"
            />
          </div>
        </form>
      </div>
    </div>
  );
}

