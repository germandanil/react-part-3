import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { errorModalOff } from "../../reducer/actions";

export default function ErrorModal() {
    const isError = useSelector(state=>state.error.isError);
    const textError = useSelector(state=>state.error.errorText);
    const dispatch = useDispatch()
    function closeForm(){
        dispatch(errorModalOff());
    }
    if(isError)
        return(
            <div className = "error-modal" onClick = {closeForm}>
                <span className = "error-modal-text">{textError}</span>
            </div>
        )
    else
        return false
}