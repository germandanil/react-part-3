import React from "react";
import Chat from "./components/Chat/chat";
import Auth from "./components/authPage/auth";
import EditPage from "./components/editPage/EditPage";
import UsersPage from "./components/userspage/usersPage";
import AddUserPage from "./components/AddUserPage/AddUserPage";
import BtnToUsersList from "./BtnToUsersList/BtnToUsersList";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import { useSelector } from "react-redux";
import EditUserPage from "./components/EditUserPage/EditUserPage";



function App() {
  const isAuthorized = useSelector(state=>state.auth.isAuthorized);
  const adminStatus = useSelector(state=>state.auth.adminStatus);
  if(!isAuthorized)
    return (
      <Router>
      <Switch>
        <Route path = "/login" exact>
          <Auth/>
        </Route>
        <Redirect to = "/login" />
      </Switch>
      </Router>
    )
  else if(!adminStatus) 
  {
    return (
      <Router>
      <Switch>
        <Route path = "/chat" exact>
          <Chat/>
        </Route>
        <Route path = "/edit/message/:id" exact>
              <EditPage/>
        </Route>
        <Redirect to = "/chat" />
      </Switch>
      </Router>
    )
  }
  else{
  return (
    <Router>
    <Switch>
      <Route path = "/chat" exact>
        <Chat/>
        <BtnToUsersList/>
      </Route>
      <Route path = "/edit/message/:id" exact>
            <EditPage/>
      </Route>
        <Route path = "/user/list" exact>
        <UsersPage/>
      </Route>
      <Route path = "/user/add" exact>
         <AddUserPage/>
       </Route>
       <Route path = "/user/edit/:id" exact>
          <EditUserPage/>
        </Route>
      <Redirect to = "/chat" />
    </Switch>
    </Router>
  )
}
}

export default App;
