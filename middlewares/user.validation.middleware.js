const createUserValid = (req, res, next) => {   
    try {
    if(!validatEmail(req.body.email))
        throw Error("Email is not gmail.com or entered incorrectly")
    if((req.body.email!==req.body.email.toLowerCase()))
        throw Error("Email must not have uppercase letters")
    if(!validatLength(req.body.password))
        throw Error("Password too short") 
    if(!validatLength(req.body.user))
        throw Error("UserName too short") 
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}


function validatEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email)
    &&(email.length - email.indexOf("@gmail.com")==10) //check gmail.com in the end
}
function validatLength(pass){
    if(pass.length<3)
        return false
    return true
}
const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        if(req.body.email&&!validatEmail(req.body.email))
            throw Error("Email is not gmail.com or entered incorrectly ")
        if(req.body.email&&(req.body.email!==req.body.email.toLowerCase()))
            throw Error("Email must not have uppercase letters")
        if(req.body.password&&!validatLength(req.body.password))
            throw Error("Password too short") 
        if(req.body.user&&!validatLength(req.body.user))
            throw Error("UserName too short") 
        } catch (err) {
            res.err = err
        } finally {
            next()
        }
}
const loginUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        if(!validatEmail(req.body.email))
            throw Error("Email is not gmail.com or entered incorrectly ")
        if((req.body.email!==req.body.email.toLowerCase()))
            throw Error("Email must not have uppercase letters")
        if(!validatLength(req.body.password))
            throw Error("Password too short") 
        } catch (err) {
            res.err = err
        } finally {
            next()
        }
}

exports.createUserValid = createUserValid
exports.updateUserValid = updateUserValid
exports.loginUserValid = loginUserValid

