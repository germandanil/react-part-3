const { BaseRepository } = require('./baseRepository');

class messageRepository extends BaseRepository {
    constructor() {
        super('messages');
    }
    deleteUsersMessages(userId) {
        return this.dbContext.remove({ userId }).write();
    }
}

exports.messageRepository = new messageRepository();