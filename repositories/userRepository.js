const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }
    create(data) {
        data.userId = this.generateId();
        const list = this.dbContext.push(data).write();
        return list.find(it => it.userId === data.userId);
    }
    update(userId, dataToUpdate) {
        return this.dbContext.find({ userId }).assign(dataToUpdate).write();
    }

    delete(userId) {
        return this.dbContext.remove({ userId }).write();
    }
}

exports.UserRepository = new UserRepository();