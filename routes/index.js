const userRoutes = require('./user');
const messageRoutes = require('./message');
module.exports = (app) => {
    app.use('/api/user', userRoutes);
    app.use('/api/message', messageRoutes);
  };