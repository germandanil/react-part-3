const { Router } = require('express')
const userService = require('../services/userService')
const { responseMiddleware } = require('../middlewares/response.middleware')
const { createUserValid, updateUserValid,loginUserValid } = require('../middlewares/user.validation.middleware')
const router = Router()


router.get('/read', (req, res, next) => {
    try {
        const data = userService.read(req.body)
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)


router.post('/login', loginUserValid,(req, res, next) => {
    try {
        if(!res.err){
        const data = userService.login(req.body)
        res.data = data
    }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.get('/all', (req, res, next) => {
    try {
        const data = userService.getAll();
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.post('/register', createUserValid,(req, res, next) => {
    try {
        if(!res.err){
            const data = userService.register(req.body)
            res.data = data
    }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.delete('/delete', (req, res, next) => {
    try {
        const data = userService.delete(req.body)
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.put('/update',updateUserValid, (req, res, next) => {
    try {
        if(!res.err){
        const data = userService.update(req.body)
        res.data = data
    }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

module.exports = router