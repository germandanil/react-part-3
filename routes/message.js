const { Router } = require('express')
const MessageService = require('../services/messageService')
const UserService = require('../services/UserService')
const { responseMiddleware } = require('../middlewares/response.middleware')

const router = Router()

router.get('/all', (req, res, next) => {
    try {
        let data = MessageService.getAll()
        data =data.map(message => {
            const messageOwner = UserService.read({userId:message.userId})

            return message ={
                ...message,
                avatar:messageOwner.avatar,
                user:messageOwner.user}
        });
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.get('/search', (req, res, next) => {
    try {
        const data = MessageService.search(req.body)
        const messageOwner = UserService.read({userId:data.userId})
        res.data = {...data,
                    avatar:  messageOwner.avatar,
                    user:    messageOwner.user
                    }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)
router.post('/create', (req, res, next) => {
    try {
        const data = MessageService.create(req.body)
        const messageOwner = UserService.read({userId:data.userId})
        res.data = {...data,
                    avatar:  messageOwner.avatar,
                    user:    messageOwner.user
                    }
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.delete('/delete', (req, res, next) => {
    try {
        const data = MessageService.delete(req.body)
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.delete('/deleteByUser', (req, res, next) => {
    try {
        const data = MessageService.deleteAllByUser(req.body)
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

router.put('/update', (req, res, next) => {
    try {
        const data = MessageService.update(req.body)
        res.data = data
    } catch (err) {
        res.err = err
    } finally {
        next()
    }
}, responseMiddleware)

module.exports = router